/*
    Purpose: Create LighteningCo tables
    Script Date: Feb 6, 2020
    Developed By: Donghyeok Seo,Maha Mahmood Shawkat, Saulo Carranza
 */


use LighteningCo
;
go

/***** Table No. 1 - Productions.Products ****/
create table Productions.Products
(
    ModelNumber varchar(65) not null primary key,   -- Primary Key
    ProductCode nvarchar(8) not null,
    SeriesID nvarchar(5) not null,  -- foreign key Series(SeriesID)
    ImpleID char(2) not null,  -- foreign key Implementations(ImpleID)
    TempID char(2) not null, -- foreign key Temperature(TempID)
    KitID char(2) not null,  -- foreign key Kits(KitID)
    BeamAngleID char(2) not null, -- foreign key BeamAngle(BeamAngleID)
    ColorID char(5) not null,   -- foreign key Colors(ColorID)
    Power tinyint not null,
    TierOnePrice smallint not null,
    TierTwoPrice smallint not null,
    MSRP smallint not null, -- Manufacturer Suggested Retail Price
    SpecSheet nvarchar(40) null, -- FIXME : Null for now. Specification sheet(Attachment)
    Reflector nvarchar(40) null,
    WhiteHousing nvarchar(40) null,
    AirTight bit not null,  -- not allowing air to escape or pass through.
    Cover nvarchar(15),  -- FIXME : Cover of Light
    Glass nvarchar(20),  -- FIXME : glass texture???
    Note1 nvarchar(60),
    Note2 nvarchar(60)
)
;
go

/***** Table No. 2 - Productions.Series ****/
create table Productions.Series
(
    SeriesID nvarchar(5) not null primary key,  -- Primary Key
    SeriesName nvarchar(15) not null,
    SeriesDescription nvarchar(70) not null
)
-- SeriesID data type? nvarchar?
-- name? description?

/***** Table No. 3 - Productions.Colors ****/
create table Productions.Colors
(
    ColorID char(5) not null primary key,   -- Primary Key
    ColorName nvarchar(15) not null,
    ColorImg  varchar(max) not null,   -- FIXME : type has to be varbinary(max) for image
)
-- name? description?

/***** Table No. 4 - Productions.Kits ****/
create table Productions.Kits
(
    KitID char(2) not null primary key,  -- Primary Key
    KitName nvarchar(25) not null
)
-- kitID

/***** Table No. 5 - Productions.Temperature ****/
create table Productions.Temperature
(
    TempID char(2) not null primary key, -- Primary Key
    TempMin smallint not null,
    TempMax smallint not null
)

-- to product table?

/***** Table No. 6 - Productions.BeamAngle ****/
create table Productions.BeamAngle
(
    BeamAngleID char(2) not null primary Key, -- Primary Key
    Angle tinyint not null
)

/***** Table No. 7 - Productions.Images ****/
create table Productions.Images
(
    ImageID smallint not null primary key,    -- Priamry Key
    Image varchar(max) not null,   -- FIXME : type has to be varbinary(max) for image
)

/***** Table No. 8 - Productions.ProductImage ****/
create table Productions.ProductImage
(
    ModelNumber varchar(65) not null,  -- Foreign Key Products (ModelNumber)
    ImageID smallint not null,    -- Foreign Key Images (ImageID)
    constraint pk_ProductImage primary key (ModelNumber,ImageID)
)

/***** Table No. 9 - Productions.Implementations ****/
create table Productions.Implementations
(
    ImpleID char(2) not null primary key,  -- Primary Key
    ImpleDescription nvarchar(50) not null  -- Implementation description
)

/***** Table No. 10 - HumanResources.Employee ****/
create table HumanResources.Employees
(
    EmployeeID int identity not null,	-- auto-generated number
    LastName nvarchar(20) not null,
    FirstName nvarchar(10) not null,
    Title nvarchar(30) not null,
    HireDate datetime not null,
    Address nvarchar(60) not null,	-- Street Number and Street Name
    City nvarchar(15) not null,
    Region nvarchar(15) null,	-- Starte or Province
    PostalCode nvarchar(10) not null,
    Country nvarchar(15) not null,
    HomePhone nvarchar(24) null,
    Notes nvarchar(1000) null,
    constraint pk_Employees primary key clustered (EmployeeID asc)
)
;
-- ID has to be nvarchar? int?

/***** Table No. 11 - Sales.Customers ****/
create table Sales.Customers
(
    CustomerID nchar(5) not null,   -- Primary Key
    CompanyName nvarchar(40) not null,
    ContactName nvarchar(30) not null,
    ContactTitle nvarchar(30) null,
    Address nvarchar(60) not null,
    City nvarchar(40) not null,
    Region nvarchar(15) null,
    PostalCode nvarchar(10) not null,
    Country nvarchar(15) not null,
    Phone nvarchar(25) not null,
    fax nvarchar(40) null,
    -- constraint constraint)name constraint_type
    constraint pk_Customers primary key clustered (customerid asc)
)
;
/***** Table No. 12 - Sales.Orders ****/
create table Sales.Orders
(
    OrderID int not null,  -- Primary Key
    CustomerID nchar(5) not null,   -- Foreign Key Customers (CustomerID)
    EmployeeID int not null,    -- Foerign key Employees (EmployeeID)
    OrderDate datetime null,
    RequiredDate datetime not null,
    ShipDate datetime null,
    ShipName nvarchar(40) null,
    ShipAddress nvarchar(60) null,
    ShipCity nvarchar(40) null,
    ShipPostalCode nvarchar(20) null,
    ShipRegion nvarchar(15) null,
    ShipCountry nvarchar(25) null,
    constraint pk_Orders primary key clustered (orderid asc)
)
;
/***** Table No. 13 - Sales.OrderDetails ****/
create table Sales.OrderDetails
(
    OrderID int not null,   -- Foreign Key Orders (OrderID)
    ModelNumber varchar(65) not null, -- Foreign Key Products (ModelNumber)
    UnitPrice money not null,
    Quantity smallint not null,
    constraint pk_OrderDetails primary key clustered (orderid asc,ModelNumber asc)
)
;
-- Product - price separate?
