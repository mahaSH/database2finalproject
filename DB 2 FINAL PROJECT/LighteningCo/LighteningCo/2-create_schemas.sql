/*
    Purpose: Create LighteningCo schema
    Script Date: Feb 6, 2020
    Developed By: Donghyeok Seo,Maha Mahmood Shawkat, Saulo Carranza
 */

use LighteningCo
;
go

/* 1) create Productions schema */
create schema Productions authorization dbo
;
go

/* 2) create HumanResources schema */
create schema HumanResources authorization dbo
;
go

/* 3) create Sales schema */
create schema Sales authorization dbo
;
go

