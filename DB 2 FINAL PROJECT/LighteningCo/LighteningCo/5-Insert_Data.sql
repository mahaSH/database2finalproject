/*
    Purpose: Insert Data
    Script Date: Feb 6, 2020
    Developed By: Donghyeok Seo,Maha Mahmood Shawkat, Saulo Carranza
 */


use LighteningCo
;
go


/* Import Products data */
BULK INSERT Productions.Products
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Products.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductsErrorRows.csv',
    TABLOCK
);

/* Import Series data */
BULK INSERT Productions.Series
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Series.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\SeriesErrorRows.csv',
    TABLOCK
);
go


/* Import BeamAngle data */
BULK INSERT Productions.BeamAngle
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\BeamAngle.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\BeamAngleErrorRows.csv',
    TABLOCK
);
go


/* Import Implementations data */
BULK INSERT Productions.Implementations
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Implementations.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImplementationsErrorRows.csv',
    TABLOCK
);
go


/* Import Temperature data */
BULK INSERT Productions.Temperature
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Temperature.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\TemperatureErrorRows.csv',
    TABLOCK
);
go


/* Import Kits data */
BULK INSERT Productions.Kits
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Kits.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\KitsErrorRows.csv',
    TABLOCK
);


/* Import Colors data */
BULK INSERT Productions.Colors
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Colors.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ColorsErrorRows.csv',
    TABLOCK
);


/* Import Images data */
BULK INSERT Productions.Images
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Images.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImagesErrorRows.csv',
    TABLOCK
);


/* Import ProductImage data */
BULK INSERT Productions.ProductImage
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\ProductImage.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
  --  ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductImageErrorRows.csv',
    TABLOCK
);


/* Import Orders data */
BULK INSERT Sales.Orders
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Orders.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrdersErrorRows.csv',
    TABLOCK
);


/* Import OrderDetails data */
BULK INSERT Sales.OrderDetails
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\OrderDetails.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrderDetailsErrorRows.csv',
    TABLOCK
);

/* Import Customers data */
BULK INSERT Sales.Customers
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Customers.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\CustomersErrorRows.csv',
    TABLOCK
);


/* Import Employees data */
BULK INSERT HumanResources.Employees
    FROM 'F:\DB 2 FINAL PROJECT\dataSample\Employees.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\EmployeesErrorRows.csv',
    TABLOCK
);

