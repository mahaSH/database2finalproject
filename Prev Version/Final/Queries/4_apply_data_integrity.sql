/*
    Purpose: Apply data integrities
    Script Date: Jan 30, 2020
    Developed By: Donghyeok Seo
 */


use LighteningCo
;
go

/***** Table No. 1 - Productions.Products ****/
-- 1) Between Productions.Products and Productions.Series
alter table Productions.Products
    add constraint fk_Products_Series foreign key(SeriesID)
    references Productions.Series (SeriesID)
;
go

-- 2) Between Productions.Products and Productions.Colors
alter table Productions.Products
    add constraint fk_Products_Colors foreign key(ColorID)
    references Productions.Colors (ColorID)
;
go

-- 3) Between Productions.Products and Productions.Temperature
alter table Productions.Products
    add constraint fk_Products_Temperature foreign key (TempID)
    references Productions.Temperature (TempID)
;
go

-- 4) Between Productions.Products and Productions.Kits
alter table Productions.Products
    add constraint fk_Products_Kits foreign key (KitID)
    references Productions.Kits (KitID)
;
go

-- 5) Between Productions.Products and Productions.BeamAngle
alter table Productions.Products
    add constraint fk_Products_BeamAngle foreign key (BeamAngleID)
    references Productions.BeamAngle (BeamAngleID)
;
go

-- 6) Between Productions.Products and Productions.Implementations
alter table Productions.Products
    add constraint fk_Products_Implementations foreign key (ImpleID)
    references Productions.Implementations (ImpleID)
;
go

--Model number form, Tier one < two < MSRP

/***** Table No. 2 - Productions.Series ****/
/***** Table No. 3 - Productions.Colors ****/
/***** Table No. 4 - Productions.Kits ****/
/***** Table No. 5 - Productions.ColorTemperature ****/
/***** Table No. 6 - Productions.BeamAngle ****/
/***** Table No. 7 - Productions.Images ****/
/***** Table No. 8 - Productions.ProductImage ****/
-- 1) Between Productions.ProductImage and Productions.Products
alter table Productions.ProductImage
    add constraint fk_ProductImage_Products foreign key (ModelNumber)
    references Productions.Products (ModelNumber)
;
go

-- 2) Between Productions.ProductImage and Productions.Images
alter table Productions.ProductImage
    add constraint fk_ProductImage_Images foreign key (ImageID)
    references Productions.Images (ImageID)
;
go

/***** Table No. 9 - Productions.Implementations ****/
/***** Table No. 10 - HumanResources.Employee ****/
/***** Table No. 11 - Sales.Customers ****/
/***** Table No. 12 - Sales.Orders ****/
-- 1) Between Sales.Orders and Sales.Customers
alter table Sales.Orders
    add constraint fk_Orders_Customers foreign key (CustomerID)
    references Sales.Customers (CustomerID)
;
go

-- 2) Between Sales.Orders and HumanResources.Employees
alter table Sales.Orders
    add constraint fk_Orders_Employees foreign key (EmployeeID)
    references HumanResources.Employees (EmployeeID)
;
go

/***** Table No. 13 - Sales.OrderDetails ****/
-- 1) Between Sales.OrderDetails and Productions.Products
alter table Sales.OrderDetails
    add constraint fk_OrderDetails_Products foreign key (ModelNumber)
    references Productions.Products (ModelNumber)
;
go

-- 2) Between Sales.OrderDetails and Sales.Orders
alter table Sales.OrderDetails
	add constraint fk_OrderDetails_Orders foreign key (OrderID)
	references Sales.Orders (OrderID)
;
go