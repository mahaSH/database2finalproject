use LighteningCo;
go

/* 1. Create a mailing list of CB clients (First, Last Name, Representative, email address) */
select CompanyName as 'Company Name', ContactName as 'Contact Name', email as 'Email address' 
from Sales.Customers
;
go

/* 2. Create a mailing list of CB employees (First, Last Name, email address) */
select CONCAT_WS(' ', FirstName, LastName) as 'Full Name', email as 'Email address'
from HumanResources.Employees
;
go

/*3- Create a view to retrieve all the products by implementation */
create view  productions.productsImplementationView
as
select 
	PI.ImpleDescription as 'implementation description',
	PP.ProductCode as 'product code',
	PP.ModelNumber as 'Model Number'
from Productions.Products as PP
	inner join Productions.Implementations as PI
	on PP.ImpleID=PI.ImpleID
;
go
select*
from productions.productsImplementationView

/* 4. Query to retrieve all the products by series (like the excel file) */ 
select 
	PS.SeriesName as 'Series', 
	PP.ModelNumber as 'Model Number', 
	PP.ProductCode as 'Product Code', 
	CONCAT_WS(', ', PS.SeriesName, PC.ColorName, PK.KitName, PP.Power, CONCAT(PT.TempMin,' - ', PT.TempMax), PBA.Angle, PP.Note1) as 'Description', 
	PP.TierOnePrice as 'Tier 1', 
	PP.TierTwoPrice as 'Tier 2', 
	PP.MSRP as 'MSRP' 

from Productions.Products as PP
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Colors as PC
		on PP.ColorID = PC.ColorID
	inner join Productions.Kits as PK
		on PP.KitID = PK.KitID
	inner join Productions.Temperature as PT
		on PP.TempID = PT.TempID
	inner join Productions.BeamAngle as PBA
		on PP.BeamAngleID = PBA.BeamAngleID
;	
go


/* 5. Query to retrieve all the products of a particular color (sort by family, product code, implementation) */
select PC.ColorName, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
from Productions.Colors as PC 
	inner join Productions.Products as PP
		on PC.ColorID = PP.ColorID
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
group by PC.ColorName, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
order by PC.ColorName asc
;
go


/* 6. Query to retrieve all the products of a specific beam angle (sort by family, product code, implementation) */
select 
	PBA.Angle as 'Beam Angle [D]', 
	PS.SeriesName, 
	PP.ModelNumber,
	PP.ProductCode, 
	PIM.ImpleDescription
from Productions.BeamAngle as PBA
	inner join Productions.Products as PP
		on PBA.BeamAngleID = PP.BeamAngleID
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
group by PBA.Angle, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
order by PBA.Angle asc
;
go