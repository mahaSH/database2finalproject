/** Reports **/

use LighteningCo;
go

/* 1.- View: How many orders were done last year*/
IF OBJECT_ID('sales.lastYearOrders','V') IS NOT NULL
    DROP VIEW sales.lastYearOrders;
go


create view sales.lastYearOrders
as
select count(orderID) as 'orders done Last year'
from sales.Orders
where year(orderDate)=year(getDate()-1)
;
go

--test
select*
from sales.lastYearOrders
;
go

/* 2.- QUERY:What percentage of the customer placed  at least 10 orders*/

/* with derived tables */

select
Count([Clients With More Than 10 Orders].[Customer ID]) as 'Number',
(select count(*) from Sales.Customers) as 'Total Number of Customers',
((Count([Clients With More Than 10 Orders].[Customer ID]) * 100) / (select count(*) from Sales.Customers)) as 'Percentage of Clients who bought more than 10 times'
from
(
select
SO.CustomerID as 'Customer ID',
COUNT(SO.OrderID) as 'Orders'
from Sales.Orders as SO
group by SO.CustomerID
having COUNT(SO.OrderID) > 10
) as [Clients With More Than 10 Orders]
;
go


/*3-FUNCTIONS :What was the greatest number of products ordered by any one individual*/
IF OBJECT_ID('Sales.fn_greatest_number_products','fn') is not null
drop function Sales.fn_greatest_number_products;
go

create function Sales.fn_greatest_number_products(@CustomerID as nvarchar(5))
returns int
as
begin
declare @result as int;

set @result =
(
select TOP 1
SUM(SOD.quantity) as 'Total quantity'
from Sales.Customers as SC
join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
where SC.CustomerID = @CustomerID
group by SC.CustomerID,SOD.ModelNumber
order by [Total quantity] desc
)

return @result;
end
;
go

IF OBJECT_ID('Sales.fn_greatest_number_modelNo','fn') is not null
drop function Sales.fn_greatest_number_modelNo;
go
create function Sales.fn_greatest_number_modelNo(@CustomerID as nvarchar(5))
returns nvarchar(65)
as
begin
declare @modelNumber as nvarchar(65);


set @modelNumber =
(
select TOP 1
SOD.ModelNumber
from Sales.Customers as SC
join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
where SC.CustomerID = @CustomerID
group by SC.CustomerID,SOD.ModelNumber
order by SUM(SOD.quantity) desc
)
return @modelNumber;
end
;
go

select CustomerID,
Sales.fn_greatest_number_modelNo(CustomerID) as 'Model Number',
Sales.fn_greatest_number_products(CustomerID) as 'Greatest Number of Products'
from Sales.Customers;
go


/*4-Function:What percentage of the products was ordered at least 10 times in the last 12 months*/

IF OBJECT_ID('sales.percentageFN','fn') is not null
	drop function sales.percentageFN;
go

--percentage function
create function sales.percentageFN
(
 @@Number1 as int,
 @@Number2 as int
 )
 returns decimal(3,2)
 as
 begin
 declare @percentage as decimal (3,2)
 select @percentage =round((@@Number1*100.0/@@Number2*1.0),10)
 return @percentage
 end
 ;
 go

-- test
select sales.percentageFN(count(*),(select count(*) from Productions.Products))
		as 'Percentage of products that was ordered at least 10 times in the last 12 months'
from (
select PP.ModelNumber
from Productions.Products as PP
	join Sales.OrderDetails as SOD on PP.ModelNumber = SOD.ModelNumber
	join Sales.Orders as SO on SOD.OrderID = SO.OrderID
where SO.OrderDate > DATEADD(yyyy,-1,GETDATE())
group by PP.ModelNumber
having count(SO.OrderID) > 10
) as p
;
go

 /*5-TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).*/
-- TODO : TEST
IF OBJECT_ID('sales.shipdateOrderDateTR','tr') is not null
	drop trigger sales.shipdateOrderDateTR;
go

create trigger sales.shipdateOrderDateTR
on sales.orders
after insert,update
as
begin
declare @shipDate as dateTime,
		@RequiredDate as dateTime
select  @shipDate =shipDate,
		@requiredDate =RequiredDate
		from inserted
		if( @shipDate>@requiredDate)
		begin
			raiserror('THE ORDER IS OVERDUE',
			10, --user severity
			1
		)
		end
end
	;
	go
--calculate the percentage
select count(orderID) as 'orders',
		(select count(*) from sales.orders) as 'total orders',
		sales.percentageFN(count(orderID),(select count(*) from sales.orders)) as 'percentage'
from sales.orders
where shipDate>requiredDate
--test the trigger
set IDENTITY_INSERT sales.orders on--turn on the IDENTITY_INSERT (if needed)
;
go

insert into Sales.Orders (orderID,CustomerID,employeeID,requiredDate,shipdate)
values(833,'ALFKI',2,'2020/01/23','2020/01/25')
;
go




/*6-PROCEDURE:What is the average quantity of orders knowing the ModelNumber*/
IF OBJECT_ID('sales.ordersAverageSP', 'P') IS NOT NULL
    DROP PROCEDURE sales.ordersAverageSP;
GO

create procedure sales.ordersAverageSP
(
	@ModelNumber as varchar(65)
	)
as
begin
select
	ModelNumber,
	AVG(Quantity) as 'Average Quantity'
from sales.orderDetails
where ModelNumber like CONCAT('%',@ModelNumber,'%')
group by ModelNumber
end
;
go
--test
execute sales.ordersAverageSP  'Bianca-24W-BI18-40K-W20-SQ-AT'
;
go


/*7. VIEW:What are the company�s peak date for orders */
IF OBJECT_ID('sales.peakDateVIEW','V') IS NOT NULL
    DROP VIEW sales.peakDateVIEW;
go

create view sales.peakDateVIEW
as
select top(10) cast(so.OrderDate as date) as 'peak days for orders',
count(so.orderID) as 'Number of Orders'
from sales.orders as so
group by so.OrderDate
order by 'Number of Orders' desc
;
go
--test
select*
from sales.peakDateView
;
go                  


-- EXTRA
-- 1. PROCEDURE:What is rank of the average quantity of orders of given Company name in this year

IF OBJECT_ID('Sales.average_quantity_per_customer', 'P') IS NOT NULL
    DROP PROCEDURE Sales.average_quantity_per_customer;
GO

create procedure Sales.average_quantity_per_customer
(
--declare the prameters
@customerID as nvarchar(5),
@rank as int output
)
as
begin

--populate the output parameters
declare @quantity as int
set @quantity =
(
select COUNT(OrderID)
from Sales.Customers as SC
join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where SC.CustomerID = @customerID -- @companyName
   and YEAR(SO.OrderDate) = YEAR(GETDATE())
)

-- Find Rank
set @rank =
(
    select COUNT(*)
    from
    (
        select distinct(QO.[Quantity of Orders]) as Quantity
        from(
            select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
            from Sales.Customers as SC
                 join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
            where --SC.CustomerID = 'ALFKI' and
                 YEAR(SO.OrderDate) = YEAR(GETDATE())
             group by SC.CompanyName
        ) as QO
    ) as R
    where R.Quantity > @quantity
)
set @rank = @rank + 1
end
;
go


select ROW_NUMBER() OVER (ORDER BY QO.[Quantity of Orders] desc) as Rank,
   QO.[Quantity of Orders] as Quantity
from(
select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
from Sales.Customers as SC
join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where YEAR(SO.OrderDate) = YEAR(GETDATE())
group by SC.CompanyName
) as QO
group by (QO.[Quantity of Orders])
order by QO.[Quantity of Orders] desc;
go

select SC.CompanyName, COUNT(SO.OrderID) as Quantity
from Sales.Customers as SC
    join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where SC.CustomerID = 'ALFKI'
    and YEAR(SO.OrderDate) = YEAR(GETDATE())
group by SC.CompanyName
;
go

declare @rank as int
execute Sales.average_quantity_per_customer 'ALFKI', @rank output
select @rank as 'ALFKI_Rank';
go


-- EXTRA(For check constraint in  Sales.Orders)
-- 2. Function : when compare ShipDate and OrderDate for check constraint
IF OBJECT_ID('Sales.fn_ShipDate_OrderDate','FN') IS NOT NULL
drop function Sales.fn_ShipDate_OrderDate;
go

create function Sales.fn_ShipDate_OrderDate
(
@ShipDate as date,
@OrderDate as date
) returns int
as
begin
declare @result as int;
set @result =
case
when (@ShipDate IS NULL) then 1
when (@ShipDate >= @OrderDate) then 1
else 0
end
return @result
end
;