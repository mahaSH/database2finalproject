/*
    Purpose: import data
    Script Date: Jan 30, 2020
    Developed By: Donghyeok Seo
 */


use LighteningCo
;
go


/* Import Products data */
BULK INSERT Productions.Products
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Products.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductsErrorRows.csv',
    TABLOCK
);

/* Import Series data */
BULK INSERT Productions.Series
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Series.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\SeriesErrorRows.csv',
    TABLOCK
);
go


/* Import BeamAngle data */
BULK INSERT Productions.BeamAngle
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\BeamAngle.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\BeamAngleErrorRows.csv',
    TABLOCK
);
go


/* Import Implementations data */
BULK INSERT Productions.Implementations
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Implementations.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImplementationsErrorRows.csv',
    TABLOCK
);
go


/* Import Temperature data */
BULK INSERT Productions.Temperature
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Temperature.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\TemperatureErrorRows.csv',
    TABLOCK
);
go


/* Import Kits data */
BULK INSERT Productions.Kits
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Kits.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\KitsErrorRows.csv',
    TABLOCK
);


/* Import Colors data */
BULK INSERT Productions.Colors
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Colors.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ColorsErrorRows.csv',
    TABLOCK
);


/* Import Images data */
BULK INSERT Productions.Images
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Images.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImagesErrorRows.csv',
    TABLOCK
);


/* Import ProductImage data */
BULK INSERT Productions.ProductImage
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\ProductImage.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
  --  ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductImageErrorRows.csv',
    TABLOCK
);


/* Import Orders data */
BULK INSERT Sales.Orders
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Orders.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrdersErrorRows.csv',
    TABLOCK
);


/* Import OrderDetails data */
BULK INSERT Sales.OrderDetails
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\OrderDetails.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrderDetailsErrorRows.csv',
    TABLOCK
);

/* Import Customers data */
BULK INSERT Sales.Customers
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Customers.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\CustomersErrorRows.csv',
    TABLOCK
);


/* Import Employees data */
BULK INSERT HumanResources.Employees
    FROM 'C:\Users\AQ-37\Documents\db_ii_group_project\Heok\dataSample\Employees.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\EmployeesErrorRows.csv',
    TABLOCK
);

