/*Add Constraints And New Columns
 Script Date: Jan 30, 2020
 Developed By: Maha M.Shawkat
*/

use LighteningCo;
go

/*1- Make Montreal DEFAULT city in customer table*/
alter table sales.customers

add constraint DF_City_Customer default ('Montreal' ) for city

/*2-create new COLUMN in customer table for email*/
alter table sales.customers
add email nvarchar(20) null

/*3-create CHECK constraint (shipDate>orderDate) in orders table*/
alter table sales.orders
add constraint ck_shipDate_OrderDate_orders
check (RequiredDate > OrderDate)

/*4-create CHECK constraint(tier1<tier2<msrp)*/
alter table productions.products
add constraint ck_prices_products check (tierOnePrice<tierTwoPrice and tierTwoPrice<msrp)

/*5-Add email address to theEmployee */
alter table humanResources.employees
add email nvarchar(20) null

/*6-Add availability to the Products table (in stock, out of stock, discontinued)*/
alter table productions.products
--drop column availability
add availability nvarchar(20)

