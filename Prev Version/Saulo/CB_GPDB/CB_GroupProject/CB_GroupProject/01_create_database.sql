/*
    Purpose: Create LighteningCo database
    Script Date: Jan 30, 2020
    Developed By: Donghyeok Seo
 */
use master
;
go

DROP DATABASE LighteningCo
;
go

CREATE DATABASE LighteningCo
on primary
(
    -- 1) rows data log
    Name = 'LighteningCo',
    -- 2) rows data initial file size
    size = 12 MB,
    -- 3) rows data auto growth size
    filegrowth = 4 MB,
    -- 4) rows data maximum file size
    maxsize = 500MB,
    -- 5) rows data path and file name
    filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\LighteningCo.mdf'
)
log on
(
    -- 1) log data logical filename
    Name = 'LighteningCo_log',
    -- 2) log data initial file size
    size = 3 MB,
    -- 3) log data auto growth size
    filegrowth = 10%,
    -- 4) log data maximum file size
    maxsize = 100MB,
    -- 5) log data path and file name
    filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\LighteningCo_log.ldf'
)
;
go

