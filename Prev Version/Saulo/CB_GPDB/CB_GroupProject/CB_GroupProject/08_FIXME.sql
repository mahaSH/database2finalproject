/** Reports **/

/* 1.- View: How many orders were done last year*/
IF OBJECT_ID('sales.lastYearOrders','V') IS NOT NULL
    DROP VIEW sales.lastYearOrders;
go


create view sales.lastYearOrders
as 
	select count(orderID) as 'orders done Last year'
	from sales.Orders
	where year(orderDate)=year(getDate()-1)
;
go

--test 
select*
from sales.lastYearOrders
;
go

/* 2.- QUERY:What percentage of the customer placed  at least 10 orders*/
-- FIXME

/* with derived tables */

select 
	Count([Clients With More Than 10 Orders].[Customer ID]) as 'Number',
	(select count(*) from Sales.Customers) as 'Total Number of Customers',
	((Count([Clients With More Than 10 Orders].[Customer ID]) * 100) / (select count(*) from Sales.Customers)) as 'Percentage of Clients who bought more than 10 times'
from 
(
	select 
		SO.CustomerID as 'Customer ID',
		COUNT(SO.OrderID) as 'Orders'
	from Sales.Orders as SO
	group by SO.CustomerID
	having COUNT(SO.OrderID) > 10
) as [Clients With More Than 10 Orders]
;
go


/*3-FUNCTIONS :What was the greatest number of products ordered by any one individual*/
IF OBJECT_ID('Sales.fn_greatest_number_products','fn') is not null
	drop function Sales.fn_greatest_number_products;
go

create function Sales.fn_greatest_number_products(@CustomerID as nvarchar(5))
returns int
as
begin
declare @result as int;

set @result =
(
	select TOP 1
		SUM(SOD.quantity) as 'Total quantity'
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
		join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
	where SC.CustomerID = @CustomerID
	group by SC.CustomerID,SOD.ModelNumber
	order by [Total quantity] desc
)

return @result;
end
;
go

IF OBJECT_ID('Sales.fn_greatest_number_modelNo','fn') is not null
	drop function Sales.fn_greatest_number_modelNo;
go
create function Sales.fn_greatest_number_modelNo(@CustomerID as nvarchar(5))
returns nvarchar(65)
as
begin
declare @modelNumber as nvarchar(65);


set @modelNumber =
(
	select TOP 1
		SOD.ModelNumber
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
		join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
	where SC.CustomerID = @CustomerID
	group by SC.CustomerID,SOD.ModelNumber
	order by SUM(SOD.quantity) desc
)
return @modelNumber;
end
;
go

select CustomerID,
	Sales.fn_greatest_number_modelNo(CustomerID) as 'Model Number',
	Sales.fn_greatest_number_products(CustomerID) as 'Greatest Number of Products'
from Sales.Customers;
go


/*4-Function:What percentage of the products was ordered at least once last year*/
--FIXME

create function sales.percentageFN
(
 @@Number1 as int,
 @@Number2 as int
 )
 returns decimal
 as
 begin
 declare @percentage as decimal
 select @percentage =round((@@Number1*100.0/@@Number2),2)
 return @percentage
 end
 ;
 go
 select count(SO.orderID)as 'number of orders',	
	 --PP.ModelNumber as 'sales percentege'
	sales.percentageFN(count(PP.ModelNumber),(select count(*) from productions.products)) as 'sales percentage'
 from sales.orders as SO
 inner join sales.OrderDetails as SOD
 on so.OrderID=SOD.OrderID
 inner Join productions.products as PP
 on SOD.ModelNumber=PP.ModelNumber
 where year(OrderDate)=year(getdate())-1
 group by  PP.ModelNumber
 having count(SO.orderID) >1
 ;
 go


 /*5-TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).*/
-- TODO : TEST
create trigger sales.shipdateOrderDateTR
on sales.orders
after insert,update
as
begin
declare @shipDate as dateTime,
		@RequiredDate as dateTime
select  @shipDate =shipDate,
		@requiredDate =RequiredDate
		from inserted
		if( @shipDate>@requiredDate)
		begin
			raiserror('the order is overdue',
	10, --user severity
	1
	)
end
end
	;
	go
--test



/*6-PROCEDURE:What is the average quantity of orders knowing the ModelNumber*/
--FIXME
create procedure sales.ordersAverageSP
(
	@ModelNumber as varchar(65)
	)
as
begin
select
	avg(Quantity) as 'average', 
	OrderID,
    ModelNumber
from sales.orderDetails
where ModelNumber like '@ModelNumber'
group by OrderID,ModelNumber
end
;
go
--test
execute sales.ordersAverageSP  'Bianca-24W-BI18-40K-W20-SQ-AT'
;
go


/*7. VIEW:What are the company�s peak days for orders */
IF OBJECT_ID('sales.peakDaysVIEW','V') IS NOT NULL
    DROP VIEW sales.peakDaysVIEW;
go

create view sales.peakDaysVIEW
as
select top(10) so.OrderDate as 'peak days for orders',
	count(so.orderID) as 'Number of Orders'
from sales.orders as so
group by so.OrderDate 
order by 'Number of Orders' desc
;
go
--test
select*
from sales.peakDaysView
;
go                   


-- EXTRA
-- 1. PROCEDURE:What is rank of the average quantity of orders of given Company name in this year

IF OBJECT_ID('Sales.average_quantity_per_customer', 'P') IS NOT NULL
    DROP PROCEDURE Sales.average_quantity_per_customer;
GO

create procedure Sales.average_quantity_per_customer
(
	--declare the prameters
	@customerID as nvarchar(5),
	@rank as int output
)
as
begin

--populate the output parameters
declare @quantity as int
set @quantity =
(
	select COUNT(OrderID)
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
	where SC.CustomerID = @customerID -- @companyName
	    and YEAR(SO.OrderDate) = YEAR(GETDATE())
)

-- Find Rank
set @rank =
(
    select COUNT(*)
    from
    (
        select distinct(QO.[Quantity of Orders]) as Quantity
        from(
            select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
            from Sales.Customers as SC
                 join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
            where --SC.CustomerID = 'ALFKI' and
                 YEAR(SO.OrderDate) = YEAR(GETDATE())
             group by SC.CompanyName
        ) as QO
    ) as R
    where R.Quantity > @quantity
)
set @rank = @rank + 1
end
;
go


select ROW_NUMBER() OVER (ORDER BY QO.[Quantity of Orders] desc) as Rank,
	    QO.[Quantity of Orders] as Quantity
from(
	select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
	where YEAR(SO.OrderDate) = YEAR(GETDATE())
	group by SC.CompanyName
) as QO
group by (QO.[Quantity of Orders])
order by QO.[Quantity of Orders] desc;
go

select SC.CompanyName, COUNT(SO.OrderID) as Quantity
from Sales.Customers as SC
    join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where SC.CustomerID = 'ALFKI'
    and YEAR(SO.OrderDate) = YEAR(GETDATE())
group by SC.CompanyName
;
go

declare @rank as int
execute Sales.average_quantity_per_customer 'ALFKI', @rank output
select @rank as 'ALFKI_Rank';
go


-- EXTRA(For check constraint in  Sales.Orders) 
-- 2. Function : when compare ShipDate and OrderDate for check constraint
IF OBJECT_ID('Sales.fn_ShipDate_OrderDate','FN') IS NOT NULL
	drop function Sales.fn_ShipDate_OrderDate;
go

create function Sales.fn_ShipDate_OrderDate
(
	@ShipDate as date,
	@OrderDate as date
) returns int
as
begin
	declare @result as int;
	set @result =
	case
		when (@ShipDate IS NULL) then 1
		when (@ShipDate >= @OrderDate) then 1
		else 0
	end
	return @result
end
;
go

