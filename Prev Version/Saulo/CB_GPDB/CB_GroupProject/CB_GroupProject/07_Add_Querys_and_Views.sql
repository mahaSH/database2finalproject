/*Add Constraints And New Columns
 Script Date: Feb 04, 2020
 Developed By: Saulo Carranza
*/

use LighteningCo
;
go

/* 1. Create a mailing list of CB clients (First, Last Name, Representative, email address) */
select CompanyName as 'Company Name', ContactName as 'Contact Name', email as 'Email address' 
from Sales.Customers
;
go


/* 2. Create a mailing list of CB employees (First, Last Name, email address) */
select CONCAT_WS(' ', FirstName, LastName) as 'Full Name', email as 'Email address'
from HumanResources.Employees
;
go


/* 3. Query to retrieve all the products by implementation (like their website) */
select PIM.ImpleDescription as 'Implementation', PP.ProductCode as 'Product Code', PP.ModelNumber as 'Model Number'
from Productions.Products as PP
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
;
go



/* 4. Query to retrieve all the products by series (like the excel file) */ 
select 
	PS.SeriesName as 'Series', 
	PP.ModelNumber as 'Model Number', 
	PP.ProductCode as 'Product Code', 
	CONCAT_WS(', ', PS.SeriesName, PC.ColorName, PK.KitName, PP.Power, CONCAT(PT.TempMin,' - ', PT.TempMax), PBA.Angle, PP.Note1) as 'Description', 
	PP.TierOnePrice as 'Tier 1', 
	PP.TierTwoPrice as 'Tier 2', 
	PP.MSRP as 'MSRP' 

from Productions.Products as PP
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Colors as PC
		on PP.ColorID = PC.ColorID
	inner join Productions.Kits as PK
		on PP.KitID = PK.KitID
	inner join Productions.Temperature as PT
		on PP.TempID = PT.TempID
	inner join Productions.BeamAngle as PBA
		on PP.BeamAngleID = PBA.BeamAngleID
;	
go

/*CHECK THIS*/
/* 5. Query to retrieve the most popular product (most quantity of sales) for each family */ 
select PS.SeriesName as 'Series', PP.ModelNumber as 'Model Number', PP.ProductCode as 'Product Code', SUM(SOD.Quantity) as 'Units Sold'
from Productions.Products as PP
	inner join Sales.OrderDetails as SOD
		on PP.ModelNumber = SOD.ModelNumber
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	 
/*having PP.ModelNumber = 
(
	select top 1 PP.ModelNumber
	from Productions.Products as PP
	order by SUM(SOD.Quantity) desc
)
*/
group by PP.ModelNumber, PS.SeriesName, PP.ProductCode
order by PS.SeriesName, SUM(SOD.Quantity) desc
;
go

/* Check This: */
/* 6. Query to retrieve the most popular product (most quantity of sales) for CB each year  */
select -- top 1 
	PP.ModelNumber as 'Model Number', 
	PP.ProductCode as 'Product Code', 
	SUM(SOD.Quantity) as 'Units Sold', 
	PS.SeriesName as 'Series Name', 
	YEAR(SO.OrderDate) as 'Year'
from Productions.Products as PP
	inner join Sales.OrderDetails as SOD
		on PP.ModelNumber = SOD.ModelNumber
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Sales.Orders as SO
		on SOD.OrderID = SO.OrderID

-- where YEAR(SO.OrderDate) in ('2018', '2019', '2020')
group by PP.ModelNumber, PP.ProductCode, PS.SeriesName, YEAR(SO.OrderDate)
order by YEAR(SO.OrderDate), SUM(SOD.Quantity) desc
;
go




/* with derived tables */
select --top 3
	[Derived Sales Per Year Details].[Model Number],
	[Derived Sales Per Year Details].[Units Sold],
	[Derived Sales Per Year Details].Year
from
(
	select
		PP.ModelNumber as 'Model Number', 
		PP.ProductCode as 'Product Code', 
		SUM(SOD.Quantity) as 'Units Sold', 
		PS.SeriesName as 'Series Name', 
		YEAR(SO.OrderDate) as 'Year'
	from Productions.Products as PP
		inner join Sales.OrderDetails as SOD
			on PP.ModelNumber = SOD.ModelNumber
		inner join Productions.Series as PS
			on PP.SeriesID = PS.SeriesID
		inner join Sales.Orders as SO
			on SOD.OrderID = SO.OrderID

-- where YEAR(SO.OrderDate) in ('2018', '2019', '2020')
	group by PP.ModelNumber, PP.ProductCode, PS.SeriesName, YEAR(SO.OrderDate)
--	order by YEAR(SO.OrderDate), SUM(SOD.Quantity) desc
) as [Derived Sales Per Year Details]
--order by [Derived Sales Per Year Details].Year
;
go







/* Check for each year */
/* 7. Query to retrieve the employee with most sales (each year?) */
select -- top 1 with ties
	HRE.EmployeeID as 'Employee ID', 
	CONCAT_WS(' ', HRE.FirstName, HRE.LastName) as 'Full Name',
	COUNT(SO.OrderID) as 'Number of Orders',
	YEAR(SO.OrderDate) as 'Order Year'
from HumanResources.Employees as HRE
	inner join Sales.Orders as SO
		on HRE.EmployeeID = SO.EmployeeID
group by HRE.EmployeeID, CONCAT_WS(' ', HRE.FirstName, HRE.LastName), SO.OrderDate 
order by YEAR(SO.OrderDate), COUNT(SO.OrderID) desc
;
go


/* 8. Query to retrieve all the products of a particular color (sort by family, product code, implementation) */
select PC.ColorName, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
from Productions.Colors as PC 
	inner join Productions.Products as PP
		on PC.ColorID = PP.ColorID
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
group by PC.ColorName, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
order by PC.ColorName asc
;
go


/* 9. Query to retrieve all the products of a specific beam angle (sort by family, product code, implementation) */
select 
	PBA.Angle as 'Beam Angle [D]', 
	PS.SeriesName, 
	PP.ModelNumber,
	PP.ProductCode, 
	PIM.ImpleDescription
from Productions.BeamAngle as PBA
	inner join Productions.Products as PP
		on PBA.BeamAngleID = PP.BeamAngleID
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
group by PBA.Angle, PS.SeriesName, PP.ModelNumber ,PP.ProductCode, PIM.ImpleDescription
order by PBA.Angle asc
;
go

/* 2-Write and execute a query on the products, implementatios, order details that returns the oreders price subtotal , product's seriesid, impldescript,when the price is higher than2000$(query)*/


/* VIEWS */

/* CHECK WHY COALESCE GIVES NULL...*/
/* 1. View list of CB clients (re-sellers) (First, Last Name, Representative, Address, City, Postal Code, Country) */

alter view Sales.ReSellersView
as
	select 
		SC.CompanyName as 'Company Name',
		SC.ContactName as 'Contact Name',
		SC.ContactTitle as 'Contact Title',
		CONCAT_WS(', ', SC.Address, SC.City, Coalesce(SC.Region, ' '), SC.PostalCode) as 'Address',
		SC.Country as 'Country'
	from Sales.Customers as SC 
;
go

select *
from Sales.ReSellersView
;
go

/* 2. View to retrieve all the product information by product code (or model number) */

create view Productions.ProductsView
as
select 
	PS.SeriesName as 'Series',
	PP.ModelNumber as 'Model Number',
	PP.ProductCode as 'Product Code',
	PP.Power as 'Power',
	CONCAT_WS(' - ', PT.TempMin, PT.TempMax) as 'Color Temperature',
	PBA.Angle as 'Bean Angle',
	PK.KitName as 'Kit Name',
	PIM.ImpleDescription as 'Implementation',
	PC.ColorName as 'Color',
	PP.Note1 as 'Additional Notes'--,
	-- PP.Note2 as 'Note2',
	-- PP.Availability as 'Availability',
	-- PIMA.Image as 'Photo(s)',
from Productions.Products as PP
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Temperature as PT
		on PP.TempID = PT.TempID
	inner join Productions.BeamAngle as PBA
		on PP.BeamAngleID = PBA.BeamAngleID
	inner join Productions.Kits as PK
		on PP.KitID = PK.KitID
	inner join Productions.Implementations as PIM
		on PP.ImpleID = PIM.ImpleID
	inner join Productions.Colors as PC
		on PP.ColorID = PC.ColorID
	--inner join Productions.Images as PIMA
	--	on PP.ImageID = PIMA.ImageID
;
go

/* 3. View 	to retrieve the list of distributors by city (display company name, city, country, contact info) */
create view Sales.ReSellersByCityView
as
select 
	SC.Country as 'Country',
	SC.City as 'City',
	SC.CompanyName as 'Company Name',
	SC.ContactName as 'Contact Name',
	SC.ContactTitle as 'Contact Title',
	SC.Phone as 'Phone',
	coalesce(SC.email, ' - ') as 'email',
	CONCAT_WS(', ', SC.Address, Coalesce(SC.Region, ' '), SC.PostalCode) as 'Address'
from Sales.Customers as SC 
;
go

/* 4. VIEW to list informaion about the orders that have shipDate inthe first quarter of the year */
create view Sales.FirstQuarterSalesView
as
select 
	SO.OrderID as 'Order ID',
	SO.OrderDate as 'Order Date',
	(SOD.UnitPrice * SOD.Quantity) as 'SubTotal'
from Sales.Orders as SO
	inner join Sales.OrderDetails as SOD
		on SO.OrderID = SOD.OrderID
where OrderDate between '1/1/2020' and '03/31/2020'
;
go

-- MAYBE ADD A TOTAL FOR THE 1ST QUARTER?


/* Usage Reports */
/*1. FUNCTION: How many orders are done  in a given year*/
create function Sales.getOrdersInAYear
(
	@OrderID as int,
	@OrderDate as datetime
)
returns int
as
	begin
		-- declare the return variable
		declare @OrdersInAYear as int
		-- compute the return value
		select @OrdersInAYear = COUNT(@OrderID)
		where year(@OrderDate) = '2019'
		-- returns the result
		return @OrdersInAYear
	end
;
go

/*2. QUERY:What percentage of the customer placed  at least one order*/
select 
	SO.CustomerID as 'Customer ID',
	COUNT(SO.OrderID) as 'Orders'
	 
from Sales.Orders as SO
	inner join Sales.Customers as SC
		on SO.CustomerID = SC.CustomerID
group by SO.CustomerID
order by COUNT(SO.OrderID) asc
;
go

select
	SO.CustomerID as 'Customer ID',
	COUNT(SO.OrderID) as 'Orders',
	(COUNT(SO.OrderID) / (COUNT(SC.CustomerID))) as 'Percentage'
from Sales.Orders as SO
	inner join Sales.Customers as SC
		on SO.CustomerID = SC.CustomerID
group by SO.CustomerID
--order by COUNT(SO.OrderID) asc

;
go

/*
What is the perecentage of UnitsInStock comparing to UnitsOnOrder. Create this query and save it as 18_qryUnitPercentage.

select  UnitsInStock, UnitsOnOrder, ([UnitsInStock]/([UnitsInStock] + [UnitsOnOrder])) as 'Percentage'
from Production.Products
where UnitsOnOrder > 0
;
go



*/

/*3. VIEW:What was the greatest number of products  ordered by any one reseller*/
create view Sales.GreatestOrderByResellerView
as
select 
	SC.CompanyName as 'Re Seller', 
	SO.OrderID as 'Order ID',
	SOD.Quantity as 'Quantity' 
from Sales.Orders as SO
	inner join Sales.Customers as SC
		on SO.CustomerID = SC.CustomerID
	inner join Sales.OrderDetails as SOD
		on SO.OrderID = SOD.OrderID
group by SC.CompanyName, SOD.Quantity, SO.OrderID  
order by SOD.Quantity desc
;
go

/*4. PROCEDURE:What percentage of the products was ordered at least once last year*/

/*5. TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).*/
create trigger Sales.OrderOverdueTr
on Sales.Orders
after insert, update
as

/*
/* create a trigger, notifyCustomerTR, that displays a message when anuone modifies on inserts data into the table Sales. Customers
*/
/* SYNTAX: create_Objet_type.onject_Name*/
create trigger Sales.NotifyCustomerChangesTr
on Sales.Customers
after insert, update
as 
	raiserror('Cusomer Table was modified',
	10,  -- user severity
	1 -- state

;
go

*/

/*6. PROCEDURE:What is the average quantity of orders*/


/*7. VIEW:What are the company�s peak days for orders */
create view PeakDaysView
as
select 
	COUNT(SO.OrderID) as 'Order ID', 
	DateName(weekday, SO.RequiredDate) as 'Required Day'
from Sales.Orders as SO
group by DateName(weekday, SO.RequiredDate)
order by DateName(weekday, SO.RequiredDate) desc
;
go


select 
	OrderID as 'Order',
	OrderDate as 'Order Date',
	year(OrderDate) as 'Order Year',
	month(OrderDate) as 'Order Month',
	day(OrderDate) as 'Order Day',
	DatePart(year, OrderDate) as 'Year Date Part',
	DatePart(month, OrderDate) as 'Month Date Part',
	DatePart(day, OrderDate) as 'Day Date Part',
	DatePart(weekday, OrderDate) as 'Weekday Date Part',
	DatePart(DayOfYear, OrderDate) as 'Day of the Year Date Part',
	DateName(weekday, OrderDate) as 'Day Name',
	EOMONTH(OrderDate) as 'End of month'
from Sales.Orders
;
go



/*
/*  Using Nested (sub) Queries 
	Script Date: January 29, 2020
	Developed by: Your Name
*/ 

/* add a statement that specifies the script runs in the context of the master database */

-- switch to FoodCo2019 database 
use Safeway20H1
;
go -- include end of batch markers (go statement)

-- some of the T-SQL date functions
select 
	OrderID as 'Order',
	OrderDate as 'Order Date',
	year(OrderDate) as 'Order Year',
	month(OrderDate) as 'Order Month',
	day(OrderDate) as 'Order Day',
	DatePart(year, OrderDate) as 'Year Date Part',
	DatePart(month, OrderDate) as 'Month Date Part',
	DatePart(day, OrderDate) as 'Day Date Part',
	DatePart(weekday, OrderDate) as 'Weekday Date Part',
	DatePart(DayOfYear, OrderDate) as 'Day of the Year Date Part',
	DateName(weekday, OrderDate) as 'Day Name',
	EOMONTH(OrderDate) as 'End of month'
from Sales.Orders
;
go

/* return all orders placed between 2019 and 2020 */
select OrderID, OrderDate
from Sales.Orders
where year(OrderDate) between '2019' and '2020'
-- where year(OrderDate) in('2019', '2020')
-- where OrderDate between '1/1/2019' and '12/31/2020'
;
go

/*some of T-SQL logical functions */
/* isNumeric(expression) - returns 1 when the input expression evaluates to a valid data type. Otherwise ir returns 0. */
select FirstName, LastName, PostalCode
from HumanResources.Employees
;
go

/* return the list of employees and the postal code with numeric values */
select FirstName, LastName, PostalCode
from HumanResources.Employees
where ISNUMERIC(PostalCode) = 1
;
go

/* IIF(expression) - Inmediate IF function that returns one of the two values depending on whethever the boolean expression evalates to true or false
SYNTAX: iif((expression), 'true_value', 'false_value')
e.g. iif((gender = 'f'), 'female', 'Male')
*/

/* return the value 'Low Price' id the product unit price is less than $50, otherwise return "high price" */
select 
	ProductID as 'Product', 
	ProductName as 'Product Name', 
	UnitPrice as 'Unit Price', 
	iif((UnitPrice < 50), 'Low Price', 'High Price') as 'Price Status'
from Production.Products
;
go

/* CASE expression evaluates a list of items and returns one of multiple result expression*/
select 
	ProductID as 'Product', 
	ProductName as 'Product Name', 
	UnitPrice as 'Unit Price', 
	'Price Range' =
		case
			-- when condition then expression
			when UnitPrice = 0 then 'Out of Stock'
			when UnitPrice < 50 then 'Unit Price is under $50'
			when UnitPrice between 50 and 250 then 'Unit Price is under $250'
			when UnitPrice between 251 and 1000 then 'Unit Price is under $1000'
			else 'UnitPrice is over $1000'
		end
from Production.Products
;
go


/* Choose function returns value at the specified index from a list of values
SYNTAX: choose(index, value1, value2, ...) where index is one-based (so, it starts in '1')*/
select choose(2, 'Manager', 'Developper', 'Programmer', 'Analyst', 'Tester')
;
go

/* replace category id value by a letter */
select 
	CategoryName, 
	CategoryID, 
	CHOOSE(CategoryID, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I')
from Production.Categories
;
go

/* some of the T-SQL string functions */

/* return the customer full address */
select 
	CustomerID, 
	CompanyName, 
	(Address + ' ' + City + ' ' + Region + ' ' + PostalCode + ' ' + Country) as 'Customer Address'
from Sales.Customers
;
go

-- isNull function
select 
	CustomerID, 
	CompanyName, 
	(Address + ' ' + City + ' ' + isNull(Region, '')  + ' ' + PostalCode + ' ' + Country) as 'Customer Address'
from Sales.Customers
;
go


-- concat function
select 
	CustomerID, 
	CompanyName, 
	concat(Address, ' ', City, ' ', Region , ' ', PostalCode, ' ', Country) as 'Customer Address'
from Sales.Customers
;
go

-- concat_ws (apply to SQL Server 2017 and higher versions)
select 
	CustomerID, 
	CompanyName, 
	concat_ws(' ', Address, City, Region, PostalCode, Country) as 'Customer Address'
from Sales.Customers
;
go

-- coalesce function evluates the argument in order and returns the current value of the first expression that initially doesn't evaluate to null
select 
	CustomerID, 
	CompanyName, 
	concat(Address, ' ', City, ' ', coalesce(Region, '-') , ' ', PostalCode, ' ', Country) as 'Customer Address'
from Sales.Customers
;
go


/*
	left([column_name], number)
	right([column_name], number)
	substring([column_name], start, lenght)
	lcase() | lower()
	ucase() | upeer()
*/

/* return the identification number based in the first 3 characters of employee first-name, and 4 character of the last name */

select upper(concat(left(FirstName, 3), left(LastName, 4))), FirstName, LastName
from HumanResources.Employees
;
go

/* return the customer company name and the telephne area code in Canada and USA */
select CompanyName, City, Phone, SUBSTRING([Phone], 2, 3) as 'Area Code'
from Sales.Customers
where Country in('Canada', 'USA')
;
go


/* some of the T-SQL conversion functions */

/* Cast() and Convert() functions allow you to convert one data type ti anirher. If the data types are incompatuble, cast will return an error */

select SYSDATETIME() as 'System Data and Time'
;
go

select cast(sysdatetime() as int)
;
go

select cast('20200130' as int)
;
go

-- convert() function
select convert(char(10), current_timestamp, 103) as 'British/French Style',
convert(char(10), current_timestamp, 104) as 'German Style',
convert(char(10), current_timestamp, 101) as 'North American Style',
convert(char(10), current_timestamp, 105) as 'Italian Style'
;
go


-- parse() function
-- SYNTAX: parse(string_value as data_type [using culture])

select
parse('06/05/2020' as datetime using 'en-us') as 'US Date Style',
parse('06/05/2020' as datetime using 'fr-fr') as 'French Date Style',
parse('06/05/2020' as datetime using 'en-ca') as 'Canada Englifh Date Style',
parse('06/05/2020' as datetime using 'fr-ca') as 'Canada French Date Style'
;

-- using try_parse(string_value as data_type [using culture])
select
	try_parse('13/9/2020' as datetime using 'en-ca') as 'Canada English Date Style'
;
go

-- SQL Server supports up to 32 international languages 
execute sp_helplanguage
;
go

-- return the local language identifier id, use @@LangID
select @@LANGID as 'Language ID'
;
go

set language 'Italian'
;
go

select @@LANGID as 'Language ID'
;
go

set language 'Italian'
declare @today as datetime
set @today = '2/10/2020'
select datename(month, @today) as 'Month Name'
;
go

set language 'Italian'
declare @today as datetime
set @today = GETDATE()
select datename(month, @today) as 'Month Name'
;
go


set language 'Simplified Chinese'
declare @today as datetime
set @today = GETDATE()
select datename(month, @today) as 'Month Name'
;
go

execute sp_helplanguage 'Simplified Chinese'
;
go

execute sp_helplanguage 'Spanish'
;
go

set language 'Spanish'
declare @today as datetime
set @today = GETDATE()
select datename(month, @today) as 'Month Name'
;
go

set language 'English'
;
go


execute sp_help 'Sales.Orders'
;
go

/* concatenate a string to an order number */
select ISNULL(N'SO' + convert(nvarchar(8), OrderID), N'*** ERROR ***') as 'Sales Order Number' -- convert because we are combining string and integer values, and that is no possible.
from Sales.Orders
;
go
*/